-----------------------------------------------------------------------------------------
--
-- main.lua
--
-----------------------------------------------------------------------------------------

local widget = require("widget")

-- Set background to white.
display.setDefault("background", 255 / 255, 255 / 255, 255 / 255)

local color = {}
color.red = {255, 0, 0}
color.orange = {255, 165, 0}
color.yellow = {255, 255, 0}
color.green = {0, 255, 0}
color.blue = {0, 0, 255}
color.purple = {128, 0, 128}

local sizeCircle = {}
sizeCircle.small = 30
sizeCircle.medium = 40
sizeCircle.large = 50

local sizeRect = {}
sizeRect.small = {60, 36}
sizeRect.medium = {80, 48}
sizeRect.large = {100, 60}

local sizeTriangle = {}
sizeTriangle.small = {-20, -25, -20, 25, 30, 0}
sizeTriangle.medium = {-30, -35, -30, 35, 40, 0}
sizeTriangle.large = {-40, -45, -40, 45, 50, 0}

local itemLocX = 0.5
local itemLocY = 0.2

-- Init `item`.
local item = display.newCircle(display.contentWidth * itemLocX, display.contentHeight * itemLocY, sizeCircle.medium)
item:setFillColor(color.orange[1] / 255, color.orange[2] / 255, color.orange[3] / 255)

local columnData = {
  {
    align = "left",
    width = 100,
    startIndex = 2,
    labels = { "small", "medium", "large" }
  },
  {
    align = "left",
    width = 110,
    startIndex = 2,
    labels = { "red", "orange", "yellow", "green", "blue", "purple" }
  },
  {
    align = "left",
    startIndex = 1,
    labels = { "circle", "rectangle", "triangle" }
  }
}

-- Create a PickerWheel widget.
local pickerWheel = widget.newPickerWheel(
  {
    x = display.contentCenterX - 10,
    y = display.contentCenterY + 70,
    columns = columnData,
    fontSize = 18,
    columnColor = {180 / 255, 255 / 255, 255 / 255},
  }
)

-- The event listener for `pickerWheel`.
local onValueSelected = function ()
  item:removeSelf()

  local values = pickerWheel:getValues()

  local _size = values[1].value
  local _color = values[2].value
  local _shape = values[3].value

  if _shape == "circle" then
    item = display.newCircle(display.contentWidth * itemLocX, display.contentHeight * itemLocY, sizeCircle[_size])
  elseif _shape == "rectangle" then
    item = display.newRoundedRect(display.contentWidth * itemLocX, display.contentHeight * itemLocY,
      sizeRect[_size][1], sizeRect[_size][2], 10)
  elseif _shape == "triangle" then
    item = display.newPolygon(display.contentWidth * itemLocX, display.contentHeight * itemLocY,
      {sizeTriangle[_size][1], sizeTriangle[_size][2],
        sizeTriangle[_size][3], sizeTriangle[_size][4],
        sizeTriangle[_size][5], sizeTriangle[_size][6]})
  end

  item:setFillColor(color[_color][1] / 255, color[_color][2] / 255, color[_color][3] / 255)
end

-- Update `item` by current selection.
timer.performWithDelay(100, onValueSelected, -1)
